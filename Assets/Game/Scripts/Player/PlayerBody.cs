﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBody : MonoBehaviour
{
	public int Coins;
	public HealthSystem HealthSystem;

	public GameObject DeathParticles;
	public GameObject StepsParticles;
	
	void Start () 
	{
	}
	
	void Update () 
	{
		
	}

	public void CreateImpact(int d, int h)
	{
		
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Coin")
		{
			var coin = other.gameObject.GetComponent<Coin>();
			Game.Instance.Coins += coin.Collect();
			Game.Instance.Ui.SetCoins(Game.Instance.Coins);
		}

		if (other.gameObject.tag == "Spike" || other.gameObject.tag == "Lava")
		{
			Game.Instance.PauseOnDeath();
			HealthSystem.TakeDamage(1);
			var p = Instantiate(DeathParticles, transform.position, transform.rotation);
			Destroy(p, 1);
		}

		if (other.gameObject.tag == "Level")
		{
			var level = other.gameObject.GetComponent<Level>();
			Game.Instance.SetLevel(level.LevelReached());
		}

		if (other.gameObject.tag == "LavaRiver")
		{
			var river = other.gameObject.GetComponent<LavaRiver>();
			river.LavaGo(); 
		}		
		
		if (other.gameObject.tag == "EndGame")
		{
			Game.Instance.EndGameResult();
		}
	}
}
