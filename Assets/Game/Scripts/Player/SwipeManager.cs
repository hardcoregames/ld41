﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeManager : MonoBehaviour
{
    public enum SwipeDirection
    {
        None = 0,
        Left = 1,
        Right = 2,
        Up = 4,
        Down = 8,

        LeftDown = 9,
        LeftUp = 5,
        RightDown = 10,
        RightUp = 6,
    }

    public SwipeDirection Direction;
    public Transform RightHand;
    public Transform LeftHand;
    public float SwipeLenght;
    private Vector3 _rightHandPosition;
    private Vector3 _leftHandPosition;
    private float _swipeResX = 0.2f;
    private float _swipeResY = 0.2f;
    private float _swipeResZ = 0.5f;
    private bool _attack = false;
	void Start()
    {
		
	}
	
	void Update()
	{
        Direction = SwipeDirection.None;

	    if (Input.GetAxis("RightTrigger") > 0 && !_attack)
	    {
            Debug.Log("Right Trigger Down");
	        _attack = true;
	        _rightHandPosition = RightHand.transform.position;
	    }
        else if (Input.GetAxis("RightTrigger") <= 0 && _attack)
        {
            _attack = false;
            Debug.Log("Right Trigger Up");

            var deltaPosition = _rightHandPosition - RightHand.transform.position;
            Debug.Log("Delta = " + deltaPosition + "; _rightHandPosition = " + _rightHandPosition + "; RightHandRealPosition = " + RightHand.transform.position);

            if (Mathf.Abs(deltaPosition.x) > SwipeLenght)
            {
                // swipe x
                Direction |= (deltaPosition.x > 0) ? SwipeDirection.Left : SwipeDirection.Right;
                Debug.Log("X = " + Direction);

            }

            if (Mathf.Abs(deltaPosition.y) > SwipeLenght)
            {
                // swipe y
                Direction |= (deltaPosition.y > 0) ? SwipeDirection.Down : SwipeDirection.Up;
                Debug.Log("Y = " + Direction);

            }

            if (Mathf.Abs(deltaPosition.z) > SwipeLenght)
            {
                // swipe z
                //Direction = (deltaPosition.y > 0) ? SwipeDirection.Up : SwipeDirection.Down;
            }

            Debug.Log("** Swipe Direction = " + Direction + " **");
        }
        /*
	    if (IsSwiping(SwipeDirection.Left))
	    {
	        Debug.Log("Swipe Left");
	    }	    
        if (IsSwiping(SwipeDirection.Right))
	    {
	        Debug.Log("Swipe Right");
	    }	    
        if (IsSwiping(SwipeDirection.Up))
	    {
	        Debug.Log("Swipe Up");
	    }	    
        if (IsSwiping(SwipeDirection.Down))
	    {
	        Debug.Log("Swipe Down");
	    }
         */
	}

    public bool IsSwiping(SwipeDirection dir)
    {
        return (Direction & dir) == dir;
    }﻿
}
