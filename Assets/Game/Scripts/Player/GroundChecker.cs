﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChecker : MonoBehaviour
{

	private ParticleSystem _particleSystem;
	
	void Start ()
	{
		_particleSystem = GetComponent<ParticleSystem>();
	}
	
	void Update () 
	{
		
	}

	public void PlayParticle()
	{
		_particleSystem.Play();
	}
}
