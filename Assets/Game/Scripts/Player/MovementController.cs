﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace VRPlayer
{
    [RequireComponent(typeof(CharacterController))]

    public class MovementController : MonoBehaviour
    {
        public enum RotationType
        {
            PieChart,
            FreeLook,
            FreeLookPieChart
        }
        [Header("Rotation Settings")]
        public RotationType RotationControlType;
        [Header("Speed Settings")]
        public float WalkSpeed = 3f;
        public float RunSpeed = 5f;
        [Header("Crouch Settings")]
        public float CrouchSpeed = 2f;
        public float CrouchHeight;
        [Header("Jump Settings")]
        public float JumpSpeed = 10;
        public float GravityMultiplier = 2;
        [Header("PieChart rotation angle")]
        public float RotationAngle = 30f;

        private Camera _camera;
        private CharacterController _characterController;
        private Transform _characterTransform;
        private Vector3 _moveDirection = Vector3.zero;
        private bool _canRotate;
        private bool _isRuning;
        private bool _isCrouching;
        private bool _isJump;
        private bool _isJumping;
        private bool _isPreviouslyGrounded;
        private float _speed;

        #region VRDisable Look
        [Header("Mouse Sens")]
        public float XSensitivity = 4f;
        public float YSensitivity = 4f;

        private Quaternion _characterRotation;
        private Quaternion _cameraRotation;

        private const float MinimumX = -90f;
        private const float MaximumX = 90f;

        #endregion

        private void Awake()
        {
            _characterController = GetComponent<CharacterController>();
        }

        private void Start()
        {
            _camera = Camera.main;
            _characterTransform = transform;

            // Mouse look переменные
            _characterRotation = _characterTransform.localRotation;
            _cameraRotation = _camera.transform.localRotation;
        }

        private void Update()
        {
            Movement();
            Rotation();
            LookDirectionRotation();
            FullRotate();
        }

        private void FixedUpdate()
        {
        }

        #region Move

        private void Movement()
        {
            CheckMove();
            CheckJump();
            //CheckCrouch();

            var input = GetInput(debug:true);
            
            // Включаем бег
            if (Input.GetButtonDown("LeftStickClick")) _isRuning = true;

            // Определяем скорость ходьбы в зависимости от состояния
            _speed = _isRuning ? RunSpeed : WalkSpeed;
            if (_isCrouching) _speed = CrouchSpeed;

            // Определяем Вектор направления движения
            Vector3 desiredMove = (transform.forward * input.y) + (transform.right * input.x);
            //Vector3 desiredMove = (_camera.transform.forward * input.y) + (transform.right * input.x);
            RaycastHit hitInfo;
            Physics.SphereCast(transform.position, _characterController.radius, Vector3.down, out hitInfo,
                               _characterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
            desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

            // Собираем вектор передвижения 
            _moveDirection.x = desiredMove.x*_speed; // left-right
            _moveDirection.z = desiredMove.z*_speed; // forward-backwward
            //_moveDirection.y = Physics.gravity.y; 

            // up-down
            if (_characterController.isGrounded)
            {
                _moveDirection.y = -JumpSpeed;
                if (_isJump)
                {
                    Debug.Log("Jump");
                    _moveDirection.y = JumpSpeed;
                    // Звук прыжка
                    _isJump = false;
                    _isJumping = true;
                }
            }
            else
            {
                _moveDirection += Physics.gravity * GravityMultiplier * Time.deltaTime;
            }

            _characterController.Move(_moveDirection * Time.deltaTime);
        }

        private bool CheckMove()
        {
            // TO DO
            // скорость бега должна регулироваться от стика
            if (GetInput() == Vector2.zero)
            {
                _isRuning = false;
                return false;
            }
            return true;
        }

        void CheckJump()
        {
            if (!_isJump)
            {
                _isJump = false;//Input.GetButtonDown("Jump");
                if (_isJump &&_isCrouching)
                {
                    StopCrouch();
                    _isJump = false;
                }
            }

            if (!_isPreviouslyGrounded && _characterController.isGrounded)
            {
                // Звук падения можно здесь сделать
                _moveDirection.y = 0f;
                _isJumping = false;
            }

            if (!_characterController.isGrounded && !_isJumping && _isPreviouslyGrounded)
            {
                _moveDirection.y = 0f;
            }

            _isPreviouslyGrounded = _characterController.isGrounded;
        }

        #endregion

        #region Crouch

        void CheckCrouch()
        {
            if (Input.GetButtonDown("Crouch"))
            {
                if (_isCrouching)
                    StopCrouch();
                else
                    Crouch();
            }
        }

        private void Crouch()
        {

            _characterController.height = _characterController.height/2;
            //_characterController.center = new Vector3(0, _characterController.height, 0);
            //_camera.transform.localPosition = new Vector3(_camera.transform.localPosition.x, _camera.transform.localPosition.y - _characterController.height / 2, _camera.transform.localPosition.z);
            _isCrouching = true;
        }

        private void StopCrouch()
        {
            _characterController.height = _characterController.height * 2;
            //_characterController.center = new Vector3(0, 0, 0);
            //_camera.transform.position = new Vector3(_camera.transform.localPosition.x, _camera.transform.localPosition.y + _characterController.height * 2, _camera.transform.localPosition.z);

            _isCrouching = false;
        }

        #endregion

        #region Rotation

        private void Rotation()
        {
            switch (RotationControlType)
            {
                case RotationType.PieChart:
                    {
                        CheckCanRotation();
                        if (_canRotate) PieChartRotation();
                    }
                    break;
                case RotationType.FreeLook:
                    {
                        _canRotate = true;
                        FreeLookRotation();
                    }
                    break;
                // For Test on Gamepad and Mouse combo
                case RotationType.FreeLookPieChart:
                    {
                        CheckCanRotation();
                        if (_canRotate) PieChartRotation();
                        HorizontalLookRotation();
                    }
                    break;
            }
        }

        // PieChart
        private void PieChartRotation()
        {
            var xInput = GetInput("Right").x;
            if (xInput == 0.0f) return;
            if (xInput < 0f) xInput = -1;
            if (xInput > 0f) xInput = 1;

            var rotY = xInput * RotationAngle;
            _characterTransform.Rotate(Vector3.up, rotY, Space.World);
            _canRotate = false;
        }

        // Поворот тела в сторону в которую смотрит камера
        private void LookDirectionRotation()
        {
            if (_canRotate && (Input.GetButtonDown("RightStickClick") || Input.GetMouseButtonDown(2)))
                _characterTransform.Rotate(Vector3.up, 
                    AngleSigned(_characterTransform.forward, _camera.transform.forward, Vector3.up), Space.World);
        }

        private void CheckCanRotation()
        {
            if (!_canRotate && (GetInput("Right") == Vector2.zero))
            {
                Debug.Log("Can rotate");
                _canRotate = true;
                StopCoroutine(FullRotateAction());
            }
        }

        private void FullRotate()
        {
            if (!_canRotate) return;
            if (GetInput("Right").y <= -0.5f)
            {
                _canRotate = false;
                StartCoroutine(FullRotateAction());
            }
        }

        IEnumerator FullRotateAction()
        {
            yield return new WaitForSeconds(1);
            if (!_canRotate && GetInput("Right").y <= -0.5f)
            {
                _characterTransform.Rotate(Vector3.up, 180, Space.World);
            }
        }

        // MouseLook
        public void FreeLookRotation()
        {
            float yRot = Input.GetAxis("Mouse X") * XSensitivity;
            float xRot = Input.GetAxis("Mouse Y") * YSensitivity;

            _characterRotation *= Quaternion.Euler(0f, yRot, 0f);
            _cameraRotation *= Quaternion.Euler(-xRot, 0f, 0f);
            _cameraRotation = ClampRotationAroundXAxis(_cameraRotation);

            _characterTransform.localRotation = _characterRotation;
            _camera.transform.localRotation = _cameraRotation;
        }        
        
        // For test only
        // TO_DO delete later
        public void HorizontalLookRotation()
        {
            float yRot = Input.GetAxis("Mouse X") * XSensitivity;
            _cameraRotation *= Quaternion.Euler(0f, yRot, 0f);
            _camera.transform.localRotation = _cameraRotation;

        }

        #endregion

        #region Helpers

        private Vector2 GetInput(string prefix = "", bool debug = false)
        {
            Vector2 input = new Vector2
            {
                x = Input.GetAxis(prefix + "Horizontal"),
                y = Input.GetAxis(prefix + "Vertical")
            };
            if (input.sqrMagnitude > 1) input.Normalize();
            return input;
        }

        private Vector2 GetInputRaw(string prefix = "")
        {
            Vector2 input = new Vector2
            {
                x = Input.GetAxisRaw(prefix + "Horizontal"),
                y = Input.GetAxisRaw(prefix + "Vertical")
            };
            if (input.sqrMagnitude > 1) input.Normalize();
            return input;
        }

        Quaternion ClampRotationAroundXAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

            angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

            q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }

        public static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
        {
            return Mathf.Atan2(
                Vector3.Dot(n, Vector3.Cross(v1, v2)),
                Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
        }

#endregion
    }
}
