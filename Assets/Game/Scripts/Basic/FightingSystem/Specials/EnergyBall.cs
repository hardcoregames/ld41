﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBall : MonoBehaviour
{
    public int Damage = 10;
    public float Force = 10f;

    void OnCollisionEnter2D(Collision2D coll)
    {
        Debug.Log("Weapon Collide with = " + coll.gameObject.tag);

        var body = coll.gameObject.GetComponent<Rigidbody2D>();
        if (body != null)
        {
            body.AddForce(transform.forward * Force, ForceMode2D.Force);
        }

        Destroy(gameObject);
        //coll.gameObject.SendMessage("ApplyDamage", 10);
    }
}
