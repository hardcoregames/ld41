﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComboController : MonoBehaviour
{
    public enum SpecialsType
    {
        EnergyBall,
        Dash,
        DashUp
    }

    public GameObject EnergyBall;
    public KeyCombo[] Specials;
    private SpriteRenderer _spriteRenderer;

    void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {

    }

    void Update()
    {
        if (Specials.Length > 0)
        {
            for (int i = 0; i < Specials.Length; i++)
            {
                if (Specials[i].Check(_spriteRenderer.flipX))
                {
                    CreateSpecial(Specials[i].Type);
                }
            }
        }
    }

    private void CreateSpecial(SpecialsType type)
    {
        switch (type)
        {
            case SpecialsType.EnergyBall:
            {
                Vector3 dir = _spriteRenderer.flipX ? Vector2.left : Vector2.right;
                var ballBody = Instantiate(EnergyBall, transform.position, transform.rotation).GetComponent<Rigidbody2D>();
                
                ballBody.AddForce(dir * 3, ForceMode2D.Impulse);
            }
                break;
            case SpecialsType.Dash:
                break;
        }
    }
}
