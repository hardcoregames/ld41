﻿using UnityEngine;

[System.Serializable]
public class KeyCombo
{
    public ComboController.SpecialsType Type;
    public string Name;
    public string[] Buttons;
    public float AllowedTimeBetweenButtons = 0.3f;

    private int _currentIndex = 0;
    private float _timeLastButtonPressed;

    public KeyCombo(string[] buttons)
    {
        Buttons = buttons;
    }

    /* TO-DO
     * 
     * 1) Сделать проверку на команду из двух кнопок
     *  
     */
    public bool Check(bool horizontalFlip)
    {
        // Сбросить индекс если время прошло
        if (Time.time > _timeLastButtonPressed + AllowedTimeBetweenButtons) 
            _currentIndex = 0;

        if (_currentIndex < Buttons.Length)
        {
            // Проверка кнопок. сначала крестовина а потом кнопки
            if ((Buttons[_currentIndex] == "down" && Input.GetAxisRaw("Vertical") == -1) ||
                (Buttons[_currentIndex] == "up" && Input.GetAxisRaw("Vertical") == 1) ||
                (Buttons[_currentIndex] == "left" && Input.GetAxisRaw("Horizontal") == (horizontalFlip ? 1 : -1)) ||
                (Buttons[_currentIndex] == "right" && Input.GetAxisRaw("Horizontal") == (horizontalFlip ? -1 : 1)) ||

                (Buttons[_currentIndex] != "down" && Buttons[_currentIndex] != "up" 
                && Buttons[_currentIndex] != "right" && Buttons[_currentIndex] != "left"
                && Input.GetButtonDown(Buttons[_currentIndex])))
            {
                _timeLastButtonPressed = Time.time;
                _currentIndex++;
            }

            if (_currentIndex >= Buttons.Length)
            {
                _currentIndex = 0;
                return true;
            }
            return false;
        }
        return false;
    }
}
