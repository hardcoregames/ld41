﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour 
{
    public int Damage;
    public float AttackSpeed;

    public SpriteRenderer WeaponSprite;

    protected float NextFire;
    //protected AudioSource SoundSource;
    protected Animator WeaponAnimator;
    
    private Transform _transform;
    private bool _attack;
    private BoxCollider2D _boxCollider2D;
    

    protected virtual void Awake()
    {
        _boxCollider2D = GetComponent<BoxCollider2D>();
        //SoundSource = GetComponent<AudioSource>();
        WeaponAnimator = GetComponent<Animator>();
        WeaponSprite = GetComponent<SpriteRenderer>();
    }

	void Start ()
	{
	    _transform = transform;
	}
	
	void Update ()
	{
	}

    public void Attack(bool flip)
    {
        if (CanAttack())
        {
            WeaponSprite.flipX = flip;
            _transform.localPosition = new Vector3(flip ? -0.25f : 0.25f, _transform.localPosition.y, _transform.localPosition.z);
            //SoundSource.PlayOneShot(AttackSounds[ComboCount]);
            NextFire = Time.time + AttackSpeed;
            WeaponAnimator.SetTrigger("Attack");
            _attack = true;
        }
        else
        {
            Debug.Log("Can't attack");
        }
    }

    public bool WeaponVisibilityCheck()
    {
        if (_attack && 
            !(WeaponAnimator.GetCurrentAnimatorStateInfo(0).length > WeaponAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime))
        {
            _attack = false;
            HideWeapon();
            return true;
        }

        return false;
    }

    public virtual bool CanAttack()
    {
        return Time.time > NextFire;
    }

    public virtual void TakeWeapon()
    {
        //gameObject.SetActive(true);
        _boxCollider2D.enabled = true;
        WeaponSprite.enabled = true;
    }

    public virtual void HideWeapon()
    {
        //gameObject.SetActive(false);
        _boxCollider2D.enabled = false;
        WeaponSprite.enabled = false;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        Debug.Log("Weapon Collide with = " + coll.gameObject.tag);

        if (coll.gameObject.tag == "Enemy")
        {
        }
        //coll.gameObject.SendMessage("ApplyDamage", 10);
    }
}
