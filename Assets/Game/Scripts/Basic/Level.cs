﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
      public int LevelNumber;

      public int LevelReached()
      {
            Debug.Log("Player reached level = " + LevelNumber);
            return LevelNumber;
      }
}
