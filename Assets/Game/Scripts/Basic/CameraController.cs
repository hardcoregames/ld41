﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CameraController : MonoBehaviour 
{
    public Transform Player;
	public Transform Lava;
    public float CameraSpeed = 1.5f;

	private float _shakeDuration;
	public float CameraShake = 0.27f;
	public float shakeDiver = 1.0f;
    private Transform _cameraTransform;
	private AudioController _audioController;

    private void Awake()
    {
	    _audioController = GetComponent<AudioController>();
    }

    void Start ()
	{
	    _cameraTransform = transform;
		StartCoroutine("ShakeGround");
	}
	
	
	IEnumerator ShakeGround()
	{
		while(true)
		{
			yield return new WaitForSeconds(Random.Range(1f, 5f));
			_audioController.StopPlayer();
			if (!Game.Instance.Pause)
			{
				_shakeDuration = Random.Range(0.5f, 3f);
				_audioController.PlayAudioByIndex(0,true, true);
			}
		}
	}

	public void ResetCamera()
	{
		_cameraTransform.position = new Vector3(1, 2 + Player.position.y, -10);
	}
	
	void Update ()
	{
		if (Game.Instance.Pause)
		{
			return;
		}
		
		if (_shakeDuration > 0)
		{
			_cameraTransform.position = Vector3.Lerp(transform.position,  new Vector3(1, 2 + Player.position.y, -10) + Random.insideUnitSphere * CameraShake, Time.deltaTime * CameraSpeed);    
			_shakeDuration -= Time.deltaTime * shakeDiver;
		}
		else
		{
			_audioController.StopPlayer();
			_shakeDuration = 0;
			_cameraTransform.position = Vector3.Lerp(transform.position,  new Vector3(1, 2 + Player.position.y, -10), Time.deltaTime * CameraSpeed);
		}
		
		/*
		var cameraShake = 0.0f;
	    if (Vector3.Distance(Lava.position, Player.position) <= 5)
	    {
		    cameraShake = CameraShake;
	    }
	    else if (Vector3.Distance(Lava.position, Player.position) <= 10)
	    {
		    cameraShake = CameraShake / 2;
	    }
		*/

        //_cameraTransform.position = Vector3.Lerp(transform.position,  new Vector3(1.17f, 2 + Player.position.y, -10), Time.deltaTime * CameraSpeed);    
	    //_cameraTransform.position = Vector3.Lerp(transform.position, Player.position + new Vector3(0, 1, -10), Time.deltaTime * CameraSpeed);    
	}
}
