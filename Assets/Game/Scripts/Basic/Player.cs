﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour 
{
    // индекс как в листе AudioController
    public enum PlayerAudio
    {
        Jump,
        JumpAttack,
        JumpFailed,
        Grounded,
        JumpLoop,
        TakeDamage,
        Run
    }

    public int JumpCapacity = 5;
    public int JumpCounter = 3;
    public int JumpDamage = 1;
    public float JumpSpeed = 7f;
    public float Speed;
    public float MaxFallSpeed = -10f;

    private bool _jumpPressed;
    private bool _jump;
    private Rigidbody2D _rigidbody;
    private Animator _animator;
    private SpriteRenderer _sprite;
    private Vector2 _velocity;

#region Audio

    private AudioController _audioController;
    
#endregion

    #region HealthSyst

    private HealthSystem _healthSystem;

    #endregion
#region Weapon

    private WeaponController _weapon;
    private bool _attack;

#endregion

#region Ground

    private GroundChecker _groundChecker;
    [SerializeField]
    private LayerMask _whatIsGround;
    private Transform _groundCheck;
    private Transform _topCheck;
    private bool _grounded;
    private bool _wasGrounded;
    private float _groundedRadius = .5f;

    private Transform _transform;
    
#endregion
    
    void Awake()
    {
        _groundChecker = GetComponentInChildren<GroundChecker>();
        _audioController = GetComponent<AudioController>();
        _topCheck = transform.Find("TopCheck");
        _groundCheck = transform.Find("Impacter");
        _weapon = transform.Find("Weapon").GetComponent<WeaponController>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _sprite = GetComponent<SpriteRenderer>();
        _healthSystem = GetComponent<HealthSystem>();
        _healthSystem.TakeDamageFire += TakeDamage;
        _healthSystem.DeadFire += PlayerDead;
    }

    void Start()
    {
        _transform = transform;
        ResetPlayer();

        Game.Instance.Ui.SetJumps(JumpCounter);
        Game.Instance.Ui.SetLives(_healthSystem.Health);

        _weapon.HideWeapon();
    }
	
    private void Update()
    {
        if (Game.Instance.Pause) return;

        //Attack();
        Jump();
    }

    public void PlayerDead(bool isdead)
    {
        _animator.SetBool("Ground", true);
        _animator.SetFloat("vSpeed", 0f);
        _sprite.enabled = false;  
        _audioController.StopAllPlayers();
    }
    
    public void ResetPlayer()
    {
        _sprite.enabled = true;
        _rigidbody.velocity = new Vector2(0, 0);
        JumpCounter = JumpCapacity;
        _wasGrounded = true;
        _animator.SetBool("Ground", true);
        _animator.SetFloat("vSpeed", 0f);
        Game.Instance.Ui.SetJumps(JumpCounter);
        _healthSystem.Health = 1;
    }
    
    private void TakeDamage(int damage, int health)
    {
        _audioController.PlayAudioByIndex((int)PlayerAudio.TakeDamage, true);
        Game.Instance.Ui.SetLives(health);
    }
    
    #region Attack

    private void Attack()
    {
        if (_attack)
        {
            if (_weapon.WeaponVisibilityCheck())
            {
                _attack = false;
                _animator.SetBool("Attack", _attack);
            }
            if (AttackAnimationCheck())
            {
                Debug.Log("Attack Stop");
            }
        }

        if (Input.GetButtonDown("Attack") && _weapon.CanAttack())
        {
            _attack = true;
            _animator.SetTrigger("PlayAttack");
            _animator.SetBool("Attack", _attack);
            _weapon.TakeWeapon();
            _weapon.Attack(_sprite.flipX);
        }
    }

    public bool AttackAnimationCheck()
    {
        return !_animator.GetCurrentAnimatorStateInfo(0).IsName("Attack");
    }

#endregion

    private void FixedUpdate()
    {
        if (Game.Instance.Pause) return;
        
        GroundCheck();

        var axisX = Input.GetAxisRaw("Horizontal");
        if (_attack) return;
        Move(axisX);
    }

    private void Move(float axisX)
    {
        // Fall Speed Correction
        var vY = _rigidbody.velocity.y;
        if (vY < MaxFallSpeed)
        {
            vY = MaxFallSpeed;
        }
        
        //_rigidbody.MovePosition(new Vector2(axisX * Speed, vY));
        _rigidbody.velocity = new Vector2(axisX * Speed, vY);
        _animator.SetFloat("Speed", Mathf.Abs(axisX));
        FlipCheck(axisX);
    }

    private void Jump()
    {
        if (Input.GetButtonDown("Jump"))
        {
            if (JumpCounter > 0)
            {
                _jumpPressed = true;
                _audioController.PlayAudioByIndex((int)PlayerAudio.Jump, true);
                _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, JumpSpeed);
                //_groundChecker.PlayParticle();
            }
            else
            {
                _audioController.PlayAudioByIndex((int)PlayerAudio.JumpFailed, true);
            }
        } //&& _grounded)
            
        /*
        else if (Input.GetButtonUp("Jump") && _grounded)
        {
            if (_rigidbody.velocity.y > 0)
                _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, _rigidbody.velocity.y * 0.5f);
        }
        */
    }

    private void GroundCheck()
    {
        _grounded = false;

        Collider2D[] colliders = Physics2D.OverlapPointAll(_groundCheck.position, _whatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                _grounded = true;
                // Restart Jumps
                if (!_wasGrounded)
                {
                    //_audioController.StopJumpMusic();
                    _audioController.PlayAudioByIndex((int)PlayerAudio.Grounded, true);
                    _groundChecker.PlayParticle();
                    if (JumpCounter != JumpCapacity)
                    {
                        JumpCounter = JumpCapacity;
                        Game.Instance.Ui.SetJumps(JumpCounter);
                    }
                }
            }
        }

        _wasGrounded = _grounded;
        
        // Hack for music and JumpCounter
        if (!_wasGrounded)
        {
            if (_jumpPressed)
            {
                _jumpPressed = false;
                JumpCounter--;
                Game.Instance.Ui.SetJumps(JumpCounter);
            } 
            //_audioController.PlayJumpMusic();
        }

        _animator.SetBool("Ground", _grounded);
        _animator.SetFloat("vSpeed", _rigidbody.velocity.y);
        //if (!_grounded) Debug.Log("Velocity = " + _rigidbody.velocity.y);
    }

    protected void FlipCheck(float axisX)
    {
        if (axisX < 0)
            _sprite.flipX = true;
        else if (axisX > 0)
            _sprite.flipX = false;
    }
    
    void OnCollisionEnter2D(Collision2D coll) 
    {
        if (coll.gameObject.tag == "Enemy")
        {
            var healthSyst = coll.gameObject.GetComponent<HealthSystem>();
            healthSyst.TakeDamage(JumpDamage);
            _audioController.PlayAudioByIndex((int)PlayerAudio.JumpAttack);
            _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, JumpSpeed);
            JumpCounter++;
            Game.Instance.Ui.SetJumps(JumpCounter);
        }        
        
        /*
        if (coll.gameObject.tag == "Lava"  || coll.gameObject.tag == "Spike")
        {
            Game.Instance.ResetPositions();
            //_audioController.PlayAudioByIndex((int)PlayerAudio.JumpAttack);
            _healthSystem.TakeDamage(1);
        }
        */
    }
}
