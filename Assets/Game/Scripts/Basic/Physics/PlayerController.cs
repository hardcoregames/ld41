﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : PhysicsObject
{
    public float JumpSpeed = 7f;
    public float MaxSpeed = 7f;

    private SpriteRenderer _spriteRenderer;
    private Animator _animator;

	void Awake ()
	{
	    _spriteRenderer = GetComponent<SpriteRenderer>();
	    _animator = GetComponent<Animator>();
	}

    protected override void ComputeVelocity()
    {
        var move = Vector2.zero;

        move.x = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && Grounded)
        {
            Velocity.y += JumpSpeed;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (Velocity.y > 0) Velocity.y = Velocity.y*0.5f;
        }

        // Флип спрайта
        if (move.x != 0.0f)
        {
            var flipsprite = _spriteRenderer.flipX ? move.x > 0.01f : move.x < 0.01f;
            if (flipsprite) _spriteRenderer.flipX = !_spriteRenderer.flipX;
        }

        //_animator.SetBool("grounded", Grounded);
        //_animator.SetFloat("velocityX", Math.Abs(Velocity.x)/MaxSpeed);

        TargetVelocity = move*MaxSpeed;
    }

}
