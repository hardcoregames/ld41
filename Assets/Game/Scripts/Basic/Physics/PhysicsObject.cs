﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour
{
    public float MinGroundNormalY = .65f;
    public float GravityModifier = 1f;

    protected Vector2 TargetVelocity;
    protected bool Grounded;
    protected Vector2 GroundNormal;

    protected ContactFilter2D ContactFilter;
    protected Vector2 Velocity;
    protected Rigidbody2D Body2D;
    protected RaycastHit2D[] HitBuffer = new RaycastHit2D[16];
    protected List<RaycastHit2D> HitBufferList = new List<RaycastHit2D>(16);

    protected const float MinMoveDistance = 0.001f;
    protected const float ShellRadius = 0.01f;

    protected virtual void ComputeVelocity() { }

    private void OnEnable()
    {
        Body2D = GetComponent<Rigidbody2D>();
    }

    void Start ()
    {
        ContactFilter.useTriggers = false;
        ContactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        ContactFilter.useLayerMask = true;
    }
	
	void Update ()
	{
	    TargetVelocity = Vector2.zero;
        ComputeVelocity();
	}

    private void FixedUpdate()
    {
        Velocity += GravityModifier * Physics2D.gravity * Time.deltaTime;
        Velocity.x = TargetVelocity.x;

        Grounded = false;
        var deltaPosition = Velocity * Time.deltaTime;

        // Horizontal Move
        var moveAlongGround = new Vector2(GroundNormal.y, -GroundNormal.x);
        var move = moveAlongGround*deltaPosition.x;
        Movement(move, false);
        // Vertical Move
        move = Vector2.up*deltaPosition.y;
        Movement(move, true);
    }

    private void Movement(Vector2 move, bool yMovement)
    {
        // Ground check
        var distance = move.magnitude;
        if (distance > MinMoveDistance)
        {
            var count = Body2D.Cast(move, ContactFilter, HitBuffer, distance + ShellRadius);
            HitBufferList.Clear();
            for (int i = 0; i < count; i++)
            {
                HitBufferList.Add(HitBuffer[i]);
            }

            for (int i = 0; i < HitBufferList.Count; i++)
            {
                var currentNormal = HitBufferList[i].normal;
                if (currentNormal.y > MinGroundNormalY)
                {
                    Grounded = true;
                    if (yMovement)
                    {
                        GroundNormal = currentNormal;
                        currentNormal.x = 0;
                    }
                }

                var projection = Vector2.Dot(Velocity, currentNormal);
                if (projection < 0)
                {
                    Velocity = Velocity - projection*currentNormal;
                }

                var modifiedDistance = HitBufferList[i].distance - ShellRadius;
                distance = modifiedDistance < distance ? modifiedDistance : distance;
            }
        }

        Body2D.position = Body2D.position + move.normalized * distance;
    }
}
