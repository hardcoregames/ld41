﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
	public int Capacity = 1;
	private AudioSource _audioSource;
	private ParticleSystem _particleSystem;
	private SpriteRenderer _sprite;
	private CircleCollider2D _collider2D;
	private bool _collected;

	private void Awake()
	{
		_collider2D = GetComponent<CircleCollider2D>();
		_sprite = GetComponent<SpriteRenderer>();
		_particleSystem = GetComponent<ParticleSystem>();
		_audioSource = GetComponent<AudioSource>();
	}

	void Start ()
	{
	}

	public int Collect()
	{
		_collider2D.enabled = false;
		_sprite.enabled = false;
		_particleSystem.Play();
		_collected = true;
		_audioSource.Play();
		return Capacity;
	}
	
	void Update () 
	{
		if (_collected && !_particleSystem.isPlaying && !_audioSource.isPlaying)
		{
			Destroy(gameObject);
		}
	}
}
