﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialController : MonoBehaviour 
{
    private Material _material;
    private Renderer _renderer;

	// Use this for initialization
	void Start() 
    {
        _renderer = GetComponent<Renderer>();
        _material = _renderer.material;
	}
	
	// Update is called once per frame
	void Update() 
    {
		
	}

    public void ChangeMaterial(bool hightlight)
    {
        _material.color = hightlight ? Color.blue : Color.white;
    }
}
