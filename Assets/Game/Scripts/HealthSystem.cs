﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour
{

	public int Health;
	private bool _isDead;
	
	public event ParameterHandler<int, int> TakeDamageFire;
	public event ParameterHandler<bool> DeadFire;
	
	void Start ()
	{
	}
	
	
	void Update () 
	{
			
	}

	public void TakeDamage(int damage)
	{
		Health -= damage;
		FireDamageEvent(damage);
		if (Health <= 0)
		{
			Debug.Log(gameObject.name + ": TakeDamage = " + damage);
			Health = 0;
			_isDead = true;
			DeadEvent(_isDead);
		}
	}
	
	private void FireDamageEvent(int damage)
	{
		if (TakeDamageFire != null)
		{
			Debug.Log(gameObject.name + ": FireDamageEvent = " + damage);
			TakeDamageFire(damage, Health);
		}
	}

	private void DeadEvent(bool isDead)
	{
		if (DeadFire != null)
		{
			DeadFire(isDead);
		}
	}

	public bool CheckIsDead()
	{
		return _isDead;
	}
}
