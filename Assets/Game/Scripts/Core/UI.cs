﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
	public Text Coins;
	public Text Lives;
	public Text Jumps;

	public Text EndGameResultText;
	public Text CreditsText;
	
	public Transform StartPanel;
	public Transform GameOverPanel;
	
	public void SetCoins(int value)
	{
		Coins.text = "Gold - " + value;
	}
	
	public void SetLives(int value)
	{
		//Lives.text = "Lives - " + value;
	}	
	public void SetJumps(int value)
	{
		Jumps.text = "Jumps - " + value;
	}


	public void SetEndGameResultText(int coins, int deaths)
	{
		EndGameResultText.text = "Gold - " + coins + "\nDeaths - " + deaths  + (coins >= deaths ? "\nGold accepted mortal" : "\nNot Enough gold mortal");
	}
}
