﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
	private Transform _playerSpawnPoint;
	private Transform _lavaSpawnPoint;
	public static Game Instance = null;
	public UI Ui;

	public Player Player;
	public Lava Lava;
	public CameraController CameraController;
	public bool Pause;
	public bool Death;

	public int Level = 0;
	public int Deaths = 0;
	public int Coins = 0;
	
	public List<Transform> PlayerSpawnPoints;
	public List<Transform> LavaSpawnPoints;
	public List<LavaRiver> LavaRivers;
	
	void Awake()
	{
		if (Instance == null)
			Instance = this;
		else if (Instance != this)           
			Destroy(gameObject);
		InitGame();
	}
	
	void InitGame()
	{
		if (Ui == null)
		{
			Ui = GetComponent<UI>();
		}

		Death = false;
		Pause = true;
		Ui.StartPanel.gameObject.SetActive(true);
		Ui.GameOverPanel.gameObject.SetActive(false);
	}
	
	public void RestartLevel()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void ResetGame()
	{
		foreach (var river in LavaRivers)
		{
			if (river != null)
				river.Particle.Stop();
		}
		
		Pause = true;
		Death = false;
		Ui.StartPanel.gameObject.SetActive(true);
		Ui.GameOverPanel.gameObject.SetActive(false);
		
		Player.ResetPlayer();
		Lava.StopLava = false;
		
		Lava.transform.position = LavaSpawnPoints[Level].position;
		Player.transform.position = PlayerSpawnPoints[Level].position;
		
		CameraController.ResetCamera();
	}

	public void PauseOnDeath()
	{
		Death = true;
		Pause = true;
		Deaths++;
		Ui.GameOverPanel.gameObject.SetActive(true);
	}
	
	public void StartGame()
	{
		Pause = false;
		Ui.StartPanel.gameObject.SetActive(false);
	}

	public void SetLevel(int level)
	{
		if (Level < level)
		{
			Level = level;
		}
	}

	public void EndGameResult()
	{
		Ui.SetEndGameResultText(Coins, Deaths);
		Lava.StopLava = true;
		//Game.Instance.Coins
	}
	
	void Start ()
	{
		
	}
	
	void Update () 
	{
		if (Input.GetButtonDown("Jump") && Pause)
		{
			if (Death)
			{
				//RestartLevel();
				ResetGame();
			}
			else
				StartGame();
		}
		
		if (Input.GetButtonDown("Restart"))
		{
			RestartLevel();
		}
	}
}
