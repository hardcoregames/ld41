﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
	[SerializeField]
	public List<AudioClip> AudioClips;

	public AudioSource SecondAudioSource;
	private AudioSource _audioSource;

	private bool _jump;

	private void Awake()
	{
		_audioSource = GetComponent<AudioSource>();
	}

	public void PlayAudioByIndex(int index, bool withRandomPitch = false, bool loop = false)
	{
		PlayAudioByAudiosource(_audioSource, index, withRandomPitch, loop);
	}	
	
	public void PlayAudioByIndex(AudioSource source, int index, bool withRandomPitch = false, bool loop = false)
	{
		PlayAudioByAudiosource(source, index, withRandomPitch, loop);
	}
	
	public void PlayAudioByAudiosource(AudioSource source, int index, bool withRandomPitch = false, bool loop = false)
	{
		var clip = AudioClips[index];
		if (clip != null)
		{
			if (withRandomPitch) source.pitch = Random.Range(0.8f, 1f);
			if (loop)
			{
				StopPlayer(source);
				source.loop = true;
				source.clip = clip;
				source.Play();
			}
			else
			{
				StopPlayer(source);
				source.PlayOneShot(clip);
			}
		}
	}

	public void StopPlayer(AudioSource source = null)
	{
		if (source == null) source = _audioSource;
		source.Stop();
		source.loop = false;
	}
	
	public void StopAllPlayers(AudioSource source = null)
	{
		if (SecondAudioSource != null)
			SecondAudioSource.Stop();
		_audioSource.Stop();
		_audioSource.loop = false;
	}

	public void PlayJumpMusic()
	{
		if (!_jump)
		{
			PlayAudioByIndex(SecondAudioSource,(int)Player.PlayerAudio.JumpLoop, false, true);
			_jump = true;
		}
		
	}
	
	public void StopJumpMusic()
	{
		if (_jump)
		{
			_jump = false;
			StopPlayer(SecondAudioSource);
		}
		
	}
}
