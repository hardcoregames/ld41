﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaRiver : MonoBehaviour
{

	public ParticleSystem Particle;
	
	// Use this for initialization
	void Start()
	{
		Particle = transform.parent.GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void LavaGo()
	{
		if (!Particle.isPlaying) Particle.Play();
	}
}
