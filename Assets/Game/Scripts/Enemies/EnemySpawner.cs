﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

	public GameObject EnemyPrefab;
	public float TimerCount;
	
	private Transform _transform;
	private float _timer;
	
	void Start ()
	{
		_transform = transform;
		_timer = Time.time + TimerCount;
	}
	
	void Update ()
	{
		if (_timer <= Time.time)
		{
			_timer = Time.time + TimerCount;
			CreateEnemy();
		}
	}

	private GameObject CreateEnemy()
	{
		var enemy = Instantiate(EnemyPrefab, _transform.position, Quaternion.identity);
		return enemy;
	}
}
