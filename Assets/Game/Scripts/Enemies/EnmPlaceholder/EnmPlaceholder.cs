﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnmPlaceholder : MonoBehaviour
{

	public Material BodyMaterial;
	public Material DamageMaterial;

	private SpriteRenderer _renderer;

	public float DamageTime;
	private float _damageTimer;
	private bool _isDamaged;
	
	private Transform _transform;
	
	// Use this for initialization
	void Start ()
	{
		//_agent = GetComponent<NavMeshAgent>();
		_transform = transform;
		_renderer = GetComponent<SpriteRenderer>();
		var healtSystem = GetComponent<HealthSystem>();
		healtSystem.TakeDamageFire += TakeDamage;
		healtSystem.DeadFire += Dead;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//_agent.SetDestination(Camera.main.transform.position);

		_transform.LookAt(Camera.main.transform);
		
		DamageUpdate();
	}

	void DamageUpdate()
	{
		if (_isDamaged)
		{
			if (_damageTimer < Time.time)
			{
				ChangeMaterial(false);
			}
		}
	}

	void Dead(bool isDead)
	{
		Destroy(gameObject);
	}
	
	void TakeDamage(int damage, int health)
	{
		_damageTimer = Time.time + DamageTime;
		ChangeMaterial(true);
	}

	void ChangeMaterial(bool isDamaged)
	{
		//Debug.Log(gameObject.name + ": ChangeMaterial Damaged = " + isDamaged);
		_isDamaged = isDamaged;
		_renderer.material = _isDamaged ? DamageMaterial : BodyMaterial;
	}
}
