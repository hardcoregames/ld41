﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Lava : MonoBehaviour
{
	public float Speed;
	public float MaxSpeed;
	private Rigidbody2D _rigidbody;
	public Transform PlayerTransform;
	private Transform _transform;
	public float _velocityY;

	private ParticleSystem _particleSystem;

	public bool StopLava;
	
	private void Awake()
	{
		_particleSystem = GetComponent<ParticleSystem>();
		_rigidbody = GetComponent<Rigidbody2D>();
	}

	void Start ()
	{
		_transform = transform;
		//_velocityY = Speed;
		//_rigidbody.velocity = new Vector2(0, Speed);
		_particleSystem.Play();
	}
	
	void Update () 
	{
		if (Game.Instance.Pause || StopLava)
		{
			_rigidbody.velocity = new Vector2(0, 0);
			return;
		}
		
		if (Vector3.Distance(PlayerTransform.position, _transform.position) > 5)
		{
			if (_velocityY == Speed)
			{
				_velocityY = MaxSpeed;
				_rigidbody.velocity = new Vector2(0, _velocityY);
			}
		}
		else
		{
			if (_velocityY != Speed)
			{
				_velocityY = Speed;
				_rigidbody.velocity = new Vector2(0, _velocityY);
			}
		}
		//_transform.position = Vector3.Lerp(transform.position,  new Vector3(0, transform.position.y) + Random.insideUnitSphere * 0.27f, Time.deltaTime * 2f);    

	}

	private void FixedUpdate()
	{
		//_rigidbody.velocity = new Vector2(0, Speed);
	}
}
